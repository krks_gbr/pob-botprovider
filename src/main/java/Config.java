import java.util.ArrayList;
import java.util.Arrays;


class Config {


    static class Bots{

        static String TEOL = "teol";
        static String VOLLH = "vollh";
        static String GIBS = "gibs";
        static String AUMNA = "aumna";

    }

    static String ROOT_PATH = System.getProperty("user.dir");
    static ArrayList<String> testBotNames = new ArrayList<>(Arrays.asList(Bots.TEOL));

    static ArrayList<String> botNames = new ArrayList<>(Arrays.asList(
            Bots.TEOL,
            Bots.VOLLH,
            Bots.GIBS,
            Bots.AUMNA
    ));
}
