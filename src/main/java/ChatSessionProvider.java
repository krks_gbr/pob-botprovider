import ab.Bot;
import ab.Chat;

import java.util.ArrayList;
import java.util.HashMap;



public class ChatSessionProvider {



    private HashMap<String, Chat> chatSessions = new HashMap<>();

    ChatSessionProvider(){
        this(Config.botNames);
    }

    ChatSessionProvider(String botName){
        initBot(botName);
    }

    ChatSessionProvider(ArrayList<String> botNames){
        botNames.forEach(this::initBot);
    }


    private void initBot(String name){
        Bot bot = new Bot(name);
        Chat session = new Chat(bot);
        session.multisentenceRespond("SET PREDICATES");
        chatSessions.put(name, session);
    }


    public String message(String botName, String message){
        return chatSessions.get(botName.toLowerCase()).multisentenceRespond(message);
    }


}
