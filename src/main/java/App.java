import io.socket.client.IO;
import io.socket.client.Socket;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class App {

    public static void main(String[] args) {

        try {

            init();

        } catch (URISyntaxException e) {

            e.printStackTrace();

        }

//        runTest(Config.Bots.VOLLH);
////        runTest("testbot");
//          runTest("critical");
    }

    static void init() throws URISyntaxException {

        String SOCKET_URL = "http://localhost:3000/bots";
        Socket socket = IO.socket(SOCKET_URL);


        ChatSessionProvider chat = new ChatSessionProvider();
        socket  .on(Socket.EVENT_CONNECT, objects -> {

                    System.out.println("socket connected");

                })
                .on(Socket.EVENT_RECONNECT, objects -> System.out.println("socket reconnected"))
                .on("message", objects -> {
                    System.out.println("receiving a message");
                    JSONObject message = (JSONObject)objects[0];

                    try {

                        String recipient = message.getString("recipient");
                        String sender = message.getString("sender");
                        String content = message.getString("content");
                        String botResponse = chat.message(recipient, content);

                        JSONObject reply = new JSONObject();


                        reply.put("sender", recipient);
                        reply.put("content", botResponse);

                        socket.emit("message", reply);

                    } catch (JSONException e) {

                        e.printStackTrace();

                    }

                })
                .on(Socket.EVENT_DISCONNECT, objects -> System.out.println("socket connection lost"));

        socket.connect();
        System.out.println("ready to connect at " + SOCKET_URL);
    }


    static void runTest(String botName) {

        ChatSessionProvider chat = new ChatSessionProvider(botName);
        Scanner scan = new Scanner(System.in);

        while (true) {
            String input = scan.nextLine();

            System.out.println("human: " + input);
            String reply = chat.message(botName, input);
            System.out.println();
            System.out.println("-----------------------------");
            System.out.println("bot: " + reply);
            System.out.println();
        }
    }

}
